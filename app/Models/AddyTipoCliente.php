<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyTipoCliente extends Model
{
    use HasFactory;

    protected $fillable = [
		'id',
		'nombre_tipo',
		'slug_tipo',
		'estado_tipo',
    ];
}
