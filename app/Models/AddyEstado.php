<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyEstado extends Model
{
    use HasFactory;

    protected $fillable = [
		'id',
		'nombre_estado',
		'descripcion_estado',
		'modulo_estado',
    ];
}
