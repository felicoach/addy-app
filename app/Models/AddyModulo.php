<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyModulo extends Model {

    use HasFactory;

    protected $fillable = [
        'icon_modulo',
        'nombre_modulo',
        'descripcion_modulo',
        'slug_modulo',
        'seccion_id',
        'estado_id',
    ];

}
