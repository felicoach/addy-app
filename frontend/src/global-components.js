import Vue from 'vue'
import FeatherIcon from '@core/components/feather-icon/FeatherIcon.vue'
import VuePhoneNumberInput from 'vue-phone-number-input';


Vue.component(FeatherIcon.name, FeatherIcon)
Vue.component('vue-phone-number-input', VuePhoneNumberInput);
