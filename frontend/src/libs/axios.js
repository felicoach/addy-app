import Vue from 'vue'

// axios
import axios from 'axios'


const axiosIns = axios.create({
  // You can add your headers here
  // ====================== ==========
  
   //baseURL: process.env.BASE_URL,
  baseURL: 'http://app.addy.la/',
  // baseURL: 'http://127.0.0.1:8000/',
  // timeout: 1000,
  // headers: {'Contenct-type': 'multipart/form-data'}
})


 


Vue.prototype.$http = axiosIns

export default axiosIns
