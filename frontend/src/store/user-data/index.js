export default {
  namespaced: true,
  state: {
    userData: JSON.parse(localStorage.getItem('userData')),
    userDataPersona:JSON.parse(localStorage.getItem("userDataPersona"))
  },
  getters: {},
  mutations: {
    UPDATE_USER_DATA(state, val) {
      state.userData = val
    },

    UPDATE_USER_DATA_PERSONA(state, val) {
      state.userDataPersona = val
    },
  },
  actions: {},
}
