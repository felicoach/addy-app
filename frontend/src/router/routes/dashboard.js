export default [
  {
    path: '/dashboard/analytics',
    name: 'dashboard-analytics',
    component: () => import('@/views/dashboard/analytics/Analytics.vue'),
    meta: {
      resource: 'empresas',
      action: 'read',
    },
  },
  {
    path: '/dashboard/ecommerce',
    name: 'dashboard-ecommerce',
    component: () => import('@/views/dashboard/ecommerce/Ecommerce.vue'),
    meta: {
      resource: 'empresas',
      action: 'read',
    },


  },
  // {
  //   path: '/dashboard/flm',
  //   name: 'dashboard-flm',
  //   component: () => import('@/views/dashboard/flm/flm.vue'),
  // },
]
