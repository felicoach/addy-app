export default [
  {
    path: '/apps/calendar',
    name: 'apps-calendar',
    component: () => import('@/views/apps/calendar/Calendar.vue'),
  },

  // *===============================================---*
  // *--------- EMAIL & IT'S FILTERS N LABELS -------------------------------*
  // *===============================================---*
  // {
  //   path: '/apps/email',
  //   name: 'apps-email',
  //   component: () => import('@/views/apps/email/Email.vue'),
  //   meta: {
  //     contentRenderer: 'sidebar-left',
  //     contentClass: 'email-application',
  //   },
  // },
  {
    path: '/apps/email/:folder',
    name: 'apps-email-folder',
    component: () => import('@/views/apps/email/Email.vue'),
    meta: {
      contentRenderer: 'sidebar-left',
      contentClass: 'email-application',
      navActiveLink: 'apps-email',
    },
    beforeEnter(to, _, next) {
      if (['sent', 'draft', 'starred', 'spam', 'trash'].includes(to.params.folder)) next()
      else next({ name: 'error-404' })
    },
  },
  {
    path: '/apps/email/label/:label',
    name: 'apps-email-label',
    component: () => import('@/views/apps/email/Email.vue'),
    meta: {
      contentRenderer: 'sidebar-left',
      contentClass: 'email-application',
      navActiveLink: 'apps-email',
    },
    beforeEnter(to, _, next) {
      if (['personal', 'company', 'important', 'private'].includes(to.params.label)) next()
      else next({ name: 'error-404' })
    },
  },

  // *===============================================---*
  // *--------- TODO & IT'S FILTERS N TAGS ---------------------------------------*
  // *===============================================---*
  // {
  //   path: '/apps/todo',
  //   name: 'apps-todo',
  //   component: () => import('@/views/apps/todo/Todo.vue'),
  //   meta: {
  //     contentRenderer: 'sidebar-left',
  //     contentClass: 'todo-application',
  //   },
  // },
  // {
  //   path: '/apps/todo/:filter',
  //   name: 'apps-todo-filter',
  //   component: () => import('@/views/apps/todo/Todo.vue'),
  //   meta: {
  //     contentRenderer: 'sidebar-left',
  //     contentClass: 'todo-application',
  //     navActiveLink: 'apps-todo',
  //   },
  //   beforeEnter(to, _, next) {
  //     if (['important', 'completed', 'deleted'].includes(to.params.filter)) next()
  //     else next({ name: 'error-404' })
  //   },
  // },
  // {
  //   path: '/apps/todo/tag/:tag',
  //   name: 'apps-todo-tag',
  //   component: () => import('@/views/apps/todo/Todo.vue'),
  //   meta: {
  //     contentRenderer: 'sidebar-left',
  //     contentClass: 'todo-application',
  //     navActiveLink: 'apps-todo',
  //   },
  //   beforeEnter(to, _, next) {
  //     if (['team', 'low', 'medium', 'high', 'update'].includes(to.params.tag)) next()
  //     else next({ name: 'error-404' })
  //   },
  // },

  // *===============================================---*
  // *--------- CHAT  ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/chat',
    name: 'apps-chat',
    component: () => import('@/views/apps/chat/Chat.vue'),
    meta: {
      contentRenderer: 'sidebar-left',
      contentClass: 'chat-application',
    },
  },

  // *===============================================---*
  // *--------- ECOMMERCE  ---------------------------------------*
  // *===============================================---*
  // {
  //   path: '/apps/e-commerce/shop',
  //   name: 'apps-e-commerce-shop',
  //   component: () => import('@/views/apps/e-commerce/e-commerce-shop/ECommerceShop.vue'),
  //   meta: {
  //     contentRenderer: 'sidebar-left-detached',
  //     contentClass: 'ecommerce-application',
  //     pageTitle: 'Shop',
  //     breadcrumb: [
  //       {
  //         text: 'ECommerce',
  //       },
  //       {
  //         text: 'Shop',
  //         active: true,
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/apps/e-commerce/wishlist',
  //   name: 'apps-e-commerce-wishlist',
  //   component: () => import('@/views/apps/e-commerce/e-commerce-wishlist/ECommerceWishlist.vue'),
  //   meta: {
  //     pageTitle: 'Wishlist',
  //     contentClass: 'ecommerce-application',
  //     breadcrumb: [
  //       {
  //         text: 'ECommerce',
  //       },
  //       {
  //         text: 'Wishlist',
  //         active: true,
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/apps/e-commerce/checkout',
  //   name: 'apps-e-commerce-checkout',
  //   component: () => import('@/views/apps/e-commerce/e-commerce-checkout/ECommerceCheckout.vue'),
  //   meta: {
  //     pageTitle: 'Checkout',
  //     contentClass: 'ecommerce-application',
  //     breadcrumb: [
  //       {
  //         text: 'ECommerce',
  //       },
  //       {
  //         text: 'Checkout',
  //         active: true,
  //       },
  //     ],
  //   },
  // },
  // {
  //   path: '/apps/e-commerce/:slug',
  //   name: 'apps-e-commerce-product-details',
  //   component: () => import('@/views/apps/e-commerce/e-commerce-product-details/ECommerceProductDetails.vue'),
  //   meta: {
  //     pageTitle: 'Product Details',
  //     contentClass: 'ecommerce-application',
  //     breadcrumb: [
  //       {
  //         text: 'ECommerce',
  //       },
  //       {
  //         text: 'Shop',
  //         active: true,
  //       },
  //       {
  //         text: 'Product Details',
  //         active: true,
  //       },
  //     ],
  //   },
  // },

  // *===============================================---*
  // *--------- USER ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/usuarios/',
    name: 'apps-users-list',
    component: () => import('@/views/apps/user/users-list/UsersList.vue'),
  
  },
  {
    path: '/users/view/:id',
    name: 'apps-users-view',
    component: () => import('@/views/apps/user/users-view/UsersView.vue'),
  
  },
  {
    path: '/usuario/crear',
    name: 'apps-users-add',
    component: () => import('@/views/apps/user/users-edit/UsersEdit.vue'),
    
  },


  // Empresas //
  {
    path: '/empresas',
    name: 'apps-empresa-list',
    component: () => import('@/views/apps/empresas/empresa-list/EmpresaList.vue'),
    meta: {
      resource: 'empresas',
      action: 'read',
    },


  },
  {
    path: '/empresas/preview/:id',
    name: 'apps-empresa-preview',
    component: () => import('@/views/apps/empresas/empresa-preview/EmpresaPreview.vue'),
    meta: {
      resource: 'empresas',
      action: 'read',
    },


  },
 
  {
    path: '/empresas/crear',
    name: 'apps-empresa-add',
    component: () => import('@/views/apps/empresas/empresa-add/EmpresaAdd.vue'),
    meta: {
      resource: 'empresas',
      action: 'read',
    },


  },
  


  // Personas //

  {
    path: '/modulos/',
    name: 'apps-modulo-list',
    component: () => import('@/views/apps/modulos/modulo-list/ModuloList.vue'),
  },

  {
    path: '/modulos/preview/:id',
    name: 'apps-modulo-preview',
    component: () => import('@/views/apps/modulos/modulo-preview/ModuloPreview.vue'),
  },
 
  {
    path: '/modulos/crear/',
    name: 'apps-modulo-add',
    component: () => import('@/views/apps/modulos/modulo-add/ModuloAdd.vue'),
  },
  
   // reles //

   {
    path: '/roles/',
    name: 'apps-role-list',
    component: () => import('@/views/apps/roles/role-list/RoleList.vue'),
  },

  {
    path: '/roles/preview/:id',
    name: 'apps-role-preview',
    component: () => import('@/views/apps/roles/role-preview/RolePreview.vue'),
  },
 
  {
    path: '/roles/crear',
    name: 'apps-role-add',
    component: () => import('@/views/apps/roles/role-add/RoleAdd.vue'),
  },

  // preguntas //

  {
    path: '/preguntas/',
    name: 'apps-pregunta-list',
    component: () => import('@/views/apps/preguntas/PreguntaList.vue'),
    meta: {     
      resource: 'preguntas',
      action: 'read',
    }
  },
 
  {
    path: '/preguntas/crear/:id/:tipo',
    name: 'apps-pregunta-add',
    component: () => import('@/views/apps/preguntas/PreguntaAdd.vue'),
    meta: {     
      resource: 'preguntas',
      action: 'create',
    },
    props: true 
  },

  // Referidos //
  {
    path: '/referidos/crear/',
    name: 'apps-referido-add',
    component: () => import('@/views/apps/referidos/referido-add/ReferidoAdd.vue'),
    meta: {
      resource: 'referidos',
      action: 'read',
    },
  },
  {
    path: '/referidos/editar/:id',
    name: 'apps-referido-edit',
    component: () => import('@/views/apps/referidos/referido-edit/ReferidoEdit.vue'),
    meta: {
      resource: 'referidos',
      action: 'read',
    },
  },
  {
    path: '/referidos/',
    name: 'apps-referido-list',
    component: () => import('@/views/apps/referidos/referido-list/ReferidoList.vue'),
    meta: {
      resource: 'referidos',
      action: 'read',
    },
  },
 
  // *===============================================---*
  // *--------- CRM FLM ---------------------------------*
  // *===============================================---*
  {
    path: '/apps/crm',
    name: 'apps-todo',
    component: () => import('@/views/apps/crm/Todo.vue'),
    meta: {
      contentRenderer: 'sidebar-left',
      contentClass: 'todo-application',
      resource: 'crm',
      action: 'create',
    },
  },
  {
    path: '/apps/crm/:filter',
    name: 'apps-todo-filter',
    component: () => import('@/views/apps/crm/Todo.vue'),
    meta: {
      contentRenderer: 'sidebar-left',
      contentClass: 'todo-application',
      navActiveLink: 'apps-todo',
      resource: 'crm',
      action: 'create',
    },
    beforeEnter(to, _, next) {
      if (['important', 'completed', 'deleted'].includes(to.params.filter)) next()
      else next({ name: 'error-404' })
    },
  },
  {
    path: '/apps/crm/tag/:tag',
    name: 'apps-todo-tag',
    component: () => import('@/views/apps/crm/Todo.vue'),
    meta: {
      contentRenderer: 'sidebar-left',
      contentClass: 'todo-application',
      navActiveLink: 'apps-todo',
      resource: 'crm',
      action: 'create',
    },
    beforeEnter(to, _, next) {
      if (['team', 'low', 'medium', 'high', 'update'].includes(to.params.tag)) next()
      else next({ name: 'error-404' })
    },
  },

  // *===============================================---*
  // *--------- PROPIEDADES FLM-------------------------*
  // *===============================================---*
  {
    path: '/apps/propiedades/filtros',
    name: 'apps-e-commerce-shop',
    component: () => import('@/views/apps/propiedades/e-commerce-shop/ECommerceShop.vue'),
    meta: {
      contentRenderer: 'sidebar-left-detached',
      contentClass: 'ecommerce-application',
      pageTitle: 'Propiedades',
      resource: 'inmuebles',
      action: 'read',

      breadcrumb: [
        {
          text: 'Filtros',
        },
        // {
        //   text: 'Shop',
        //   active: true,
        // },
      ],
    },
  },
  {
    path: '/apps/propiedades/favoritos',
    name: 'apps-e-commerce-wishlist',
    component: () => import('@/views/apps/propiedades/e-commerce-wishlist/ECommerceWishlist.vue'),
    meta: {
      pageTitle: 'Favoritos',
      contentClass: 'ecommerce-application',
      resource: 'inmuebles',
      action: 'read',
      breadcrumb: [
        {
          text: 'Casas',
        },
        {
          text: 'Detalles',
          active: true,
        },
      ],
    },
  },
  {
    path: '/apps/propiedades/checkout',
    name: 'apps-e-commerce-checkout',
    component: () => import('@/views/apps/propiedades/e-commerce-checkout/ECommerceCheckout.vue'),
    meta: {
      pageTitle: 'Checkout',
      contentClass: 'ecommerce-application',
      resource: 'inmuebles',
      action: 'read',
      breadcrumb: [
        {
          text: 'ECommerce',
        },
        {
          text: 'Checkout',
          active: true,
        },
      ],
    },
  },
  {
    path: '/apps/propiedades/:slug',
    name: 'apps-e-commerce-product-details',
    component: () => import('@/views/apps/propiedades/e-commerce-product-details/ECommerceProductDetails.vue'),
    meta: {
      pageTitle: 'Propiedades',
      contentClass: 'ecommerce-application',
      resource: 'inmuebles',
      action: 'read',
      breadcrumb: [
        {
          text: 'Detalles',
        },
        {
          text: 'Casas',
          active: false,
        },
        {
          text: 'Sur',
          active: true,
        },
      ],
    },
  },




]
