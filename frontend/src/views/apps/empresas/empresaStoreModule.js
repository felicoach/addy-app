import axios from '@axios'

const token = localStorage.getItem('accessToken');

const config = {
  headers: {
    'Authorization': `Bearer ${token}`,
    //'Content-Type': 'multipart/form-data'
  },
};
export default {
  namespaced: true,
  state: {
    empresa:[]
  },
  getters: {},
  mutations: {
    SET_EMPRESAS: (state, payload) => {
      state.empresa = payload;
  },
  },

  actions: {

    fetchEmpresas(ctx, queryParams) {

      return new Promise((resolve, reject) => {
        axios
          .get('/api/auth/empresas', {}, config)
          .then(response => {
            ctx.commit('SET_EMPRESAS', response.data)
            resolve(response.data)
          })
          .catch(error => reject(error))
      })
    },
    fetchEmpresa(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchClients() {
      return new Promise((resolve, reject) => {
        axios
          .get('/')
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addEmpresa(ctx, empresa) {
      console.log(empresa)
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/empresas', empresa, config)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
  },
}
