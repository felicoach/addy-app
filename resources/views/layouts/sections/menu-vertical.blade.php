<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    <li class="nav-item">

        <a class="d-flex align-items-center opcion-administrador" href="{{ url('home') }}">
            <i data-feather="home"></i>
            <span class="menu-title text-truncate" id="titulo_principal" data-i18n="Dashboards">
                Administrador
            </span>
            <span class="badge badge-light-warning badge-pill ml-auto mr-1">?</span>
        </a>

        <div class="opcion_adicional"></div>

        <ul class="menu-content">
            @can('haveaccess','permisos.habilitado.empresas')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="{{ route('permisos.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-title text-truncate" data-i18n="Invoice">
                        Empresas
                    </span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('empresas.index') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Preview">
                                Listado
                            </span>
                        </a>
                    </li>
                    @can('haveaccess','permisos.crear.empresas')
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('empresas.create') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="List">
                                Registrar Empresa
                            </span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
<!--
            <li class="nav-item">
                <a class="d-flex align-items-center" href="{{ route('permisos.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-title text-truncate" data-i18n="Invoice">
                        Permisos
                    </span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('permisos.index') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Preview">
                                Listado
                            </span>
                        </a>
                    </li>
                    <li>
                        <a class="d-flex align-items-center" href="app-invoice-list.html">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="List">
                                Crear permiso
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
-->
            @can('haveaccess','permisos.habilitado.modulos')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="#">
                    <i data-feather="circle"></i>
                    <span class="menu-title text-truncate" data-i18n="Invoice">
                        M&oacute;dulos
                    </span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('modulos.index') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Preview">
                                Listado
                            </span>
                        </a>
                    </li>
                    @can('haveaccess','permisos.crear.modulos')
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('modulos.create') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="List">
                                Crear m&oacute;dulo
                            </span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('haveaccess','permisos.habilitado.roles')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="{{ route('permisos.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-title text-truncate" data-i18n="Invoice">
                        Roles
                    </span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('roles.index') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Preview">
                                Listado
                            </span>
                        </a>
                    </li>
                    @can('haveaccess','permisos.crear.roles')
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('roles.create') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="List">
                                Crear rol
                            </span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('haveaccess','permisos.habilitado.usuarios')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="{{ route('users.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-title text-truncate" data-i18n="Invoice">
                        Usuarios
                    </span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('users.index') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Preview">
                                Listado
                            </span>
                        </a>
                    </li>
                    @can('haveaccess','permisos.crear.usuarios')
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('users.create') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="List">
                                Crear Usuario
                            </span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('haveaccess','permisos.habilitado.preguntas')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="{{ route('permisos.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-title text-truncate" data-i18n="Invoice">
                        Preguntas
                    </span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('preguntas.index') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Preview">
                                Listado
                            </span>
                        </a>
                    </li>
                    @can('haveaccess','permisos.crear.preguntas')
                    <li>
                        <a class="d-flex align-items-center" href="{{ route('preguntas.create') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="List">
                                Registrar preguntas
                            </span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
        </ul>
    </li>
    <!-- Demas opciones de los usuarios -->                
    <li class=" navigation-header">
        <span data-i18n="Apps &amp; Pages">
            OPCIONES GENERALES
        </span>
        <i data-feather="more-horizontal"></i>
    </li>
    @can('haveaccess','permisos.habilitado.personas')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="{{ route('personas.index') }}">
            <i data-feather="user"></i>
            <span class="menu-title text-truncate" data-i18n="Invoice">
                Módulo Personas
            </span>
        </a>
        <ul class="menu-content">
            <li>
                <a class="d-flex align-items-center" href="{{ route('personas.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-item text-truncate" data-i18n="Preview">
                        Listado
                    </span>
                </a>
            </li>
            @can('haveaccess','permisos.crear.personas')
            <li>
                <a class="d-flex align-items-center" href="{{ route('personas.create') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-item text-truncate" data-i18n="List">
                        Registrar Persona
                    </span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('haveaccess','permisos.habilitado.referidos')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="{{ route('referidos.index') }}">
            <i data-feather="user"></i>
            <span class="menu-title text-truncate" data-i18n="Invoice">
                Módulo Referidos
            </span>
        </a>
        <ul class="menu-content">
            <li>
                <a class="d-flex align-items-center" href="{{ route('referidos.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-item text-truncate" data-i18n="Preview">
                        Listado
                    </span>
                </a>
            </li>
            @can('haveaccess','permisos.crear.referidos')
            <li>
                <a class="d-flex align-items-center" href="{{ route('referidos.create') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-item text-truncate" data-i18n="List">
                        Registrar Referido
                    </span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('haveaccess','permisos.habilitado.bitacoras')
    <li class="nav-item">
        <a class="d-flex align-items-center" href="{{ route('bitacoras.index') }}">
            <i data-feather="user"></i>
            <span class="menu-title text-truncate" data-i18n="Invoice">
                Módulo Bitacoras
            </span>
        </a>
        <ul class="menu-content">
            <li>
                <a class="d-flex align-items-center" href="{{ route('bitacoras.index') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-item text-truncate" data-i18n="Preview">
                        Listado
                    </span>
                </a>
            </li>
            @can('haveaccess','permisos.crear.bitacoras')
            <li>
                <a class="d-flex align-items-center" href="{{ route('bitacoras.create') }}">
                    <i data-feather="circle"></i>
                    <span class="menu-item text-truncate" data-i18n="List">
                        Crear Bitacora
                    </span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    
</ul>