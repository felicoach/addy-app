@extends('layouts.app')

@section('title', 'Módulo Persona')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">

    	@include('pages.configuracion.mensajes.alertas')

        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Referidos Addy</h3>
                            <p class="card-text font-small-3">Hola! acá verás un listado con todos los referidos registrados.</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <div class="panel-busqueda">
                                <div class="row">
                                    <div class="form-group col-12 col-md-9 col-lg-9 col-sm-9 col-xl-9 col-lg-9">
                                        <label><b>Búsqueda de referidos:</b></label>
                                        <input type="text" class="form-control" name="busqueda_empresa" id="busqueda" data-modulo="empresas">
                                    </div>
                                    <div class="form-group col-12 col-md-3 col-lg-3 col-sm-3 col-xl-3 col-lg-3">
                                        <label></label>
                                        <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Búsqueda avanzada</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-responsive ">
                                    <thead>
                                        <tr>
                                        	<th>Cédula </th>
                                            <th>Foto</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Correo</th>
                                            <th>WhatsApp</th>
                                            <th>Grupo</th>
                                            <th colspan="4" style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	
	                                        @foreach($referidos as $persona)
	                                        	@if($persona->estado_persona != 1)
	                                        	<tr class="alert alert-danger">
	                                        		<td>{{ $persona->cedula_persona }}</td>
	                                        		<td>{{ $persona->foto_persona }}</td>
	                                        		<td>{{ $persona->nombres_persona }}</td>
	                                        		<td>{{ $persona->apellidos_persona }}</td>
	                                        		<td>{{ $persona->correo_persona }}</td>
	                                        		<td>{{ $persona->celular_whatsapp }}</td>
	                                        		<td>{{ $persona->nit_empresa }}</td>
	                                        		<td>
	                                                <button type="button" class="btn btn-success informacion" data-href="{{ route('personas.show', $persona->id) }}"> 
	                                                    Ver
	                                                </button>
		                                            </td>
		                                            <td>
		                                                <button type="button" class="btn btn-info edicion" data-href="{{ route('personas.edit', $persona->id) }}"> 
		                                                    Editar 
		                                                </button>
		                                            </td>

		                                            <td>
		                                                <button type="button" class="btn btn-danger eliminar" data-href="{{ route('personas.destroy', $persona->id) }}"> 
		                                                    Eliminar
		                                                </button>
		                                            </td>
	                                        	</tr>
	                                        	@else
	                                        	<tr>
	                                        		<td>{{ $persona->cedula_persona }}</td>
	                                        		<td><img src="{{ $persona->foto_persona }}" width="100px"></td>
	                                        		<td>{{ $persona->nombres_persona }}</td>
	                                        		<td>{{ $persona->apellidos_persona }}</td>
	                                        		<td>{{ $persona->correo_persona }}</td>
	                                        		<td>{{ $persona->celular_whatsapp }}</td>
	                                        		<td>{{ $persona->nit_empresa }}</td>
	                                        		<td>
	                                                <button type="button" class="btn btn-success informacion" data-href="{{ route('referidos.show', $persona->cedula_persona) }}"> 
	                                                    Ver
	                                                </button>
		                                            </td>
		                                            <td>
	                                                    <a href="{{ route('personas.edit', $persona->cedula_persona) }}">
	    	                                                <button type="button" class="btn btn-info">
	    	                                                    Editar 
	    	                                                </button>
	                                                    </a>
		                                            </td>

		                                            <td>
		                                                <button type="button" class="btn btn-danger bitacoras" data-href="{{ route('generar', $persona->cedula_persona) }}"> 
		                                                    Iniciar Bitacora
		                                                </button>
		                                            </td>
	                                        	</tr>
	                                        	@endif
	                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@include('pages.configuracion.mensajes.modales')

@endsection
