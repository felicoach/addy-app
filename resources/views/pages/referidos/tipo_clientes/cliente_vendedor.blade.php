@extends('layouts.app')

@section('title', 'Registro Referido Cliente Vendedor - Aplicativo Addy')

@section('content')

<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registrar un nuevo referido - cliente comprador</h3>
                            <p class="card-text font-small-3">Bienvenido, desde esta sección puedes registrar una persona</p>

                            <hr>

                            <form id="form_empresa" action="{{ route('referidos.store') }}" method="POST" enctype="multipart/form-data">

                                @csrf
                                @method('POST')

                                <input type="text" class="form-control" name="tipo_cliente" value="cliente_vendedor" readonly="">
                                <br>
                                <div class="form-group">
                                    <label><b> (*) Foto: </b></label>
                                	<input type="file" class="form-control" name="foto_persona">
                                </div>
                                <div class="form-group">
                                    <label><b> (*) Número de cédula: </b></label>
                                    <input type="text" class="form-control" name="numero_cedula" value="{{ old('numero_cedula') }}">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b> (*) Nombres: </b></label>
                                            <input type="text" class="form-control" name="nombres" value="{{ old('nombres') }}"> 
                                        </div>
                                        <div class="col">
                                            <label><b> (*) Apellidos: </b></label>
                                            <input type="text" class="form-control" name="apellidos" value="{{ old('apellidos') }}"> 
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b> (*) Correo electrónico: </b></label>
                                    <input type="email" class="form-control" name="correo" value="{{ old('correo') }}">
                                </div>

                                <div class="form-group">
                                    <label><b> (*) Número de WhatsApp: </b></label>
                                    <input class='form-control' type="tel" name="numero_whatsapp" id="celular_movil" value="{{ old('numero_whatsapp') }}">
                                            <span id="valid-msg" class="hide">✓ Válido</span>
                                            <span id="error-msg" class="hide"></span>
                                </div>

                                <div class="form-group">
                                    <label><b> (*) Fecha de Nacimiento: </b></label>
                                    <input type="date" class="form-control" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}">
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>(*) País:</label>
                                            <select class="form-control" id="selector"  name="codigo_pais" value="{{ old('codigo_pais') }}" >
                                                <option value="" selected=""></option>
                                                @foreach($paises as $pais)
                                                    <option value="{{ $pais->codigo }}">{{ $pais->pais }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label>(*) Ciudad:</label>
                                            <select class="form-control" id="selector_ciudad"  name="codigo_ciudad" value="{{ old('codigo_ciudad') }}" >
                                            </select>
                                        </div>
                                    </div>
                                </div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="1. ¿Cuál  es  el  rango  de  precio  de  la  compra?" readonly="">
									<textarea class="form-control" name="respuesta_1" >{{ old('respuesta_1') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="2. ¿Alguien  más  participará  en  la  toma  de  decisión  de  compra  del  inmueble?" readonly="">
									<textarea class="form-control" name="respuesta_2">{{ old('respuesta_2') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="3. ¿Quién  vivirá  o  que  uso  le  dará  al  inmueble?" readonly="">
									<textarea class="form-control" name="respuesta_3">{{ old('respuesta_3') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="4. ¿Hace  cuánto  tiempo  está  buscando  este  inmueble?" readonly="">
									<textarea class="form-control" name="respuesta_4">{{ old('respuesta_4') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="5. ¿Cuál  es  la  razón  de  su  compra?" readonly="">
									<textarea class="form-control" name="respuesta_5">{{ old('respuesta_5') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="6. ¿Vive  en  inmueble  alquilado?" readonly="">
									<textarea class="form-control" name="respuesta_6">{{ old('respuesta_6') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="7. ¿Sabe  cuándo  termina  su  contrato  de  alquiler?" readonly="">
									<textarea class="form-control" name="respuesta_7">{{ old('respuesta_7') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="8. ¿Es propietario?" readonly="">
									<textarea class="form-control" name="respuesta_8">{{ old('respuesta_8') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="9.  ¿Necesita  vender  un  inmueble  actual  antes  de  esta  compra?" readonly="">
									<select class="form-control" name="respuesta_9">
										<option value="Si">Si</option>
										<option value="Si">No</option>
									</select>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="10. ¿Ha  firmado un acuerdo de venta con algún agente de bienes raíces para  vender su  inmueble?" readonly="">
									<select class="form-control" name="respuesta_10">
										<option value="Si">Si</option>
										<option value="Si">No</option>
									</select>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="11.  ¿Pagará  en  efectivo  o  con  préstamo  bancario?" readonly="">
									<textarea class="form-control" name="respuesta_11">{{ old('respuesta_11') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="12. ¿Ya  cuenta  con  la  pre-aprobación  de  un  banco?" readonly="">
									<textarea class="form-control" name="respuesta_12">{{ old('respuesta_12') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="13. ¿Con  que  banco?" readonly="">
									<textarea class="form-control" name="respuesta_13">{{ old('respuesta_13') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="14. ¿Qué  monto  le han  pre-aprobado?" readonly="">
									<textarea class="form-control" name="respuesta_14">{{ old('respuesta_14') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="15. ¿Cuánto  cancelará  de cuota  inicial?" readonly="">
									<textarea class="form-control" name="respuesta_15">{{ old('respuesta_15') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="16. ¿Le  gustaría  que  le  recomendará  un  asesor  bancario  de  confianza?" readonly="">
									<textarea class="form-control" name="respuesta_16">{{ old('respuesta_16') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="17. En  una  escala  de  1  a  10,  donde  10  significa  que  debe  comprar  este  inmueble  lo  antes  posible  y  1  significa que  no  está  seguro  si  comprará  algo,  ¿cómo  se  calificaría? si  no  es  10  ¿Qué  se  requiere  para  que  fuera un  10? " readonly="">
									<textarea class="form-control" name="respuesta_17">{{ old('respuesta_17') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="18. ¿Para  qué  fecha  requiere  su  nuevo  inmueble?" readonly="">
									<textarea class="form-control" name="respuesta_18">{{ old('respuesta_18') }}</textarea>
								</div>

								<div class="form-group">
									<input type="text" name="preguntas[]" class="form-control" value="19. Me  encantaría  ayudarle  a  comprar  su  inmueble,  para  esto  es  conveniente  que  concertemos  una  cita  con todas  aquellas  personas  que  tomen  la  decisión,  para  poder  ayudarles  a  conseguir  lo  que  necesitan  en  el tiempo  que  lo  requieran  ¿Cuándo  le  parece  bien  que  nos  veamos? " readonly="">
									<input type="datetime-local" class="form-control" name="respuesta_19" value="{{ old('respuesta_19') }}">
								</div>

								</p>

								<center>
									<button type="submit" class="btn btn-success">Registrar Formulario</button>
								</center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

    $(document).on("change", "#selector", function () {
       
        $.ajax({
            type: 'GET',
            url: '/empresas/ciudad/' + $(this).val(),
            success: function (data) {
                $("#selector_ciudad").html(data);
            }
        });
    });
</script>
@endsection
