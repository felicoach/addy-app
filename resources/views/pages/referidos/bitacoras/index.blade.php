@extends('layouts.app')

@section('title', 'Módulo Persona')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">

    	@include('pages.configuracion.mensajes.alertas')

        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Bitacoras</h3>
                            <p class="card-text font-small-3">Hola! acá verás un listado con todos los referidos registrados.</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <div class="panel-busqueda">
                                <div class="row">
                                    <div class="form-group col-12 col-md-9 col-lg-9 col-sm-9 col-xl-9 col-lg-9">
                                        <label><b>Búsqueda de Bitacoras:</b></label>
                                        <input type="text" class="form-control" name="busqueda_empresa" id="busqueda" data-modulo="empresas">
                                    </div>
                                    <div class="form-group col-12 col-md-3 col-lg-3 col-sm-3 col-xl-3 col-lg-3">
                                        <label></label>
                                        <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Búsqueda avanzada</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-responsive ">
                                    <thead>
                                        <tr>
                                        	<th>Código Bitácora </th>
                                            <th>Descripción Bitácora</th>
                                            <th>Fecha creación Bitácora</th>
                                            <th>Creador</th>
                                            <th>Acción realizada</th>
                                            <th>Estado actual</th>
                                            <th colspan="1" style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($bitacoras as $bitacora)
                                    	<tr>
                                    		<td>{{ $bitacora->codigo_bitacora }}</td>
                                    		<td>{{ $bitacora->descripcion_bitacora }}</td>
                                    		<td>{{ $bitacora->fecha_bitacora }}</td>
                                    		<td>
                                    		@foreach($usuarios as $usuario)
        										@if(is_array($usuarios_creadores) && in_array($usuario->id, $usuarios_creadores))
        											{{ $usuario->name }}
        										@endif
                                    		@endforeach
                                    		</td>
                                    		<td>
                                    		@foreach($acciones as $accion)
        										@if(is_array($acciones_bitacora) && in_array($accion->codigo_accion, $acciones_bitacora))
        											{{ $accion->descripcion_accion }}
        										@endif
                                    		@endforeach
                                    		</td>
                                    		<td>
                                    		@foreach($estados as $estado)
        										@if(is_array($estados_bitacora) && in_array($estado->id, $estados_bitacora))
        											{{ $estado->nombre_estado }}
        										@endif
                                    		@endforeach
                                    		</td>
                                    		<td>
                                                <a href="{{ route('bitacoras.show', $bitacora->codigo_bitacora) }}">
                                                    <button type="button" class="btn btn-info"> 
                                                        Ver
                                                    </button>
                                                </a>
	                                       	</td>
	                                    </tr>
	                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@include('pages.configuracion.mensajes.modales')

@endsection
