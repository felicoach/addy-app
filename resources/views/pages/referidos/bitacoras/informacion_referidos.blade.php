<div class="row">
	<div class="col">
		<label><b>Cédula persona</b></label><br>
		{{ $persona[0]->cedula_persona }}
	</div>
	<div class="col">
		<label><b>Nombres persona</b></label><br>
		{{ $persona[0]->nombres_persona }}
	</div>
	<div class="col">
		<label><b>Apellidos persona</b></label><br>
		{{ $persona[0]->apellidos_persona }}
	</div>
	<div class="col">
	<label><b>Fecha de nacimiento</b></label><br>
		{{ $persona[0]->fecha_nacimiento }}
	</div>
</div>
<hr>
<div class="row">
	<div class="col">
		<label><b>Correo persona</b></label><br>
		{{ $persona[0]->correo_persona }}
	</div>
	<div class="col">
		<label><b>Telefono fijo</b></label><br>
		{{ $persona[0]->telefono_fijo }}
	</div>
	<div class="col">
		<label><b>Celular</b></label><br>
		{{ $persona[0]->celular_movil }}
	</div>
	<div class="col">
		<label><b>Whatsapp persona</b></label><br>
		{{ $persona[0]->celular_whatsapp }}
	</div>
</div>

<hr>
<div class="row">
	<div class="col">
		<label><b>Dirección persona</b></label><br>
		{{ $persona[0]->direccion_persona }}
	</div>
	<div class="col">
		<label><b>Pais persona</b></label><br>
		{{ $persona[0]->telefono_fijo }}
	</div>
	<div class="col">
		<label><b>Ciudad Persona</b></label><br>
		{{ $persona[0]->celular_movil }}
	</div>
	<div class="col">
		<label><b>Código Postal</b></label><br>
		{{ $persona[0]->celular_whatsapp }}
	</div>
</div>


<hr>
<div class="row">
	<div class="col">
		<label><b>cedula Agente</b></label><br>
		{{ $persona[0]->nit_empresa }}
	</div>
</div>

<hr>
<div class="row">
	<div class="col">
		<h4><b>Cuestionario:</b></h4>
	</div>
</div>

@foreach($formularios as $formulario)
<div class="row">
	<div class="container">
		<textarea class="form-control" readonly="">{{ $formulario->pregunta }}</textarea>
		<br>
		{{ $formulario->respuesta }}
	</div>
</div>
<hr>
@endforeach