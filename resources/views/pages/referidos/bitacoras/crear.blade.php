<?php date_default_timezone_set('America/Bogota'); ?>

<form id="formulario" action="{{ route('bitacoras.store') }}" method="POST">
	@method('POST')
	@csrf
	
	<div class="form-group">
		<label><b>(*) Asociar a:</b></label>
		<input type="text" class="form-control" name="cedula" value="{{ $cedula }}" readonly="">
	</div>

	<div class="form-group">
		<label><b>(*) Código Bitacora:</b></label>
		<input type="text" class="form-control" name="code_bitacora" value="{{ $codigo }}" readonly="">
	</div>

	<div class="form-group">
		<label><b>(*) Descripción de la Bitacora:</b></label>
		<textarea  name="descripcion_bitacora" class="form-control"></textarea>
	</div>

	<div class="form-group">
		<label><b>(*) Fecha de creación:</b></label>
		<input type="datetime" class="form-control" name="fecha_bitacora" value="{{ date('Y-m-d H:i:s') }}" readonly="">
	</div>

	<div class="form-group">
		<label><b>(*) Usuario creador:</b></label>
		<input type="hidden" class="form-control" name="usuario_creador" value="{{ auth()->user()->id }}">
		<input type="text" class="form-control"value="{{ auth()->user()->name }}" readonly="">
	</div>

	<div class="form-group">
		<label><b>(*) Estado Bitacora:</b></label>
		<select class="form-control" name="estado_bitacora">
			@foreach($estados as $estado)
				<option value="{{ $estado->id }}">{{ $estado->nombre_estado }}</option>
			@endforeach
		</select>
	</div>
	<br>
	<center>
		<input type="submit" class="btn btn-success" value="Crear">
	</center>
</form>