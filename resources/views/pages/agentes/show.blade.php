<div class="form-group">
	<label><b>(*) Tipo de Identificación:</b></label>
	<select class="form-control" disabled="" readonly="">
		@if($agente[0]->tipo_identificacion == 1)
		<option value="1" selected="">CC</option>
		<option value="2">NIT</option>
		<option value="3">RUT</option>
		@elseif($agente[0]->tipo_identificacion == 1)
		<option value="1" selected="">CC</option>
		<option value="2" selected="">NIT</option>
		<option value="3">RUT</option>
		@else
		<option value="1">CC</option>
		<option value="2">NIT</option>
		<option value="3" selected="">RUT</option>
		@endif
	</select>
</div>

<div class="form-group">
	<label><b>Número de Identificación:</b></label>
	<input type="number" class="form-control" name="indentificacion_tributaria" value="{{ $agente[0]->cedula_agente }}" readonly="">
</div>

<div class="form-group">
	<label><b>Nombre:</b></label>
	<input type="text" class="form-control" value="{{ $agente[0]->nombre_agente }}" readonly="">
</div>

<div class="form-group">
	<label><b>Apellidos:</b></label>
	<input type="text" class="form-control" value="{{ $agente[0]->apellido_agente }}" readonly="">
</div>

<div class="form-group">
	<label><b>Email:</b></label>
	<input type="text" class="form-control" value="{{ $agente[0]->correo_agente }}" readonly="">
</div>

<div class="form-group">
	<label><b>WhatsApp:</b></label>
	<input type="text" class="form-control" value="{{ $agente[0]->celular_agente }}" readonly="">
</div>

<div class="form-group">
	<label><b>Empresa:</b></label><br>
	@foreach($empresas as $inmobiliaria)
		@if(is_array($empresa) && in_array($inmobiliaria->id, $empresa))
			<button type="button" class="btn btn-info btn-sm" style="margin: 2px;">{{ $inmobiliaria->nombre_empresa }}</button>
        @endif
	@endforeach
</div>

<div class="form-group">
	<label><b> (*) Estado: </b></label>
	<select class="form-control" name="estado" readonly="" disabled="">
		@if($agente[0]->estado_agente == 1)
		<option value="1" selected="">Activo</option>
		<option value="2">Inactivo</option>
		@else
		<option value="1">Activo</option>
		<option value="2" selected="">Inactivo</option>
		@endif
	</select>
</div>
