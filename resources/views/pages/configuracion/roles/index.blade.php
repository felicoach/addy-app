@extends('layouts.app')

@section('title', 'Módulo Permisos (Roles) - Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">

                            <h3>M&oacute;dulo de Roles</h3>
                            <p class="card-text font-small-3">
                                Hola! acá verás un listado con todos los roles de las empresas registradas.</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>
                            <div class="panel-busqueda">
                                <div class="row">
                                    <div class="form-group col-12 col-md-9 col-lg-9 col-sm-9 col-xl-9 col-lg-9">
                                        <label><b>Búsqueda de empresas:</b></label>
                                        <input type="text" class="form-control" name="busqueda_empresa" id="busqueda" data-modulo="empresas">
                                    </div>
                                    <div class="form-group col-12 col-md-3 col-lg-3 col-sm-3 col-xl-3 col-lg-3">
                                        <label></label>
                                        <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Búsqueda avanzada</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover table-responsive ">
                                    <thead>
                                        <tr>
                                            <th>Nombre del Rol</th>
                                            <th>Slug del Rol</th>
                                            <th colspan="4" style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($roles as $rol)
                                            @if($rol->estado_rol != 1)
                                                <tr class="alert alert-danger">
                                                    <td>{{ $rol->nombre }}</td>
                                                    <td>{{ $rol->slug }}</td>
                                                    @if($rol->slug == 'administrator')
                                                        <td>
                                                            <button type="button" class="btn btn-success nodal-{xs|sm|lg|xl} informacion" data-toggle="modal" data-target="#default" data-href="{{ route('roles.show', $rol->id) }}"> 
                                                                Ver
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-info " disabled="disabled"> 
                                                                Editar
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-warning "  disabled="disabled"> 
                                                                Asignar Permisos
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger "   disabled="disabled"> 
                                                                Eliminar
                                                            </button>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <button type="button" class="btn btn-success nodal-{xs|sm|lg|xl} informacion" data-toggle="modal" data-target="#default" data-href="{{ route('roles.show', $rol->id) }}"> 
                                                                Ver
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-info edicion" data-href="{{ route('roles.edit', $rol->id) }}"> 
                                                                Editar 
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-warning asignacion" data-href="{{ route('asignacion.edit', $rol->id) }}"> 
                                                                Asignar Permisos
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger eliminar" data-href="{{ route('empresas.destroy', $rol->id) }}"> 
                                                                Eliminar
                                                            </button>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @else
                                            <tr>
                                            <td>{{ $rol->nombre }}</td>
                                            <td>{{ $rol->slug }}</td>
                                            @if($rol->slug == 'administrator')
                                                <td>
                                                    <button type="button" class="btn btn-success nodal-{xs|sm|lg|xl}" disabled=""> 
                                                        Ver
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info " disabled="disabled"> 
                                                        Editar
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-warning" disabled=""> 
                                                                Asignar Permisos
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger "   disabled="disabled"> 
                                                        Eliminar
                                                    </button>
                                                </td>
                                                @else
                                                <td>
                                                    <button type="button" class="btn btn-success nodal-{xs|sm|lg|xl} informacion" data-toggle="modal" data-target="#default" data-href="{{ route('roles.show', $rol->id) }}"> 
                                                        Ver
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-info edicion" data-href="{{ route('roles.edit', $rol->id) }}"> 
                                                        Editar 
                                                    </button>
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-warning asignacion" data-href="{{ route('asignacion.edit', $rol->id) }}"> 
                                                                Asignar Permisos
                                                    </button>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-danger eliminar" data-href="{{ route('empresas.destroy', $rol->id) }}"> 
                                                        Eliminar
                                                    </button>
                                                </td>
                                            @endif
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>

@include('pages.configuracion.mensajes.modales')



@endsection