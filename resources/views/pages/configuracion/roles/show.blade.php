
<div class="form-group">
    <label><b>Nombre del rol: </b></label>
    <input type="text" class="form-control" value="{{ $rol[0]->nombre }}" readonly="">
</div>


<div class="form-group">
    <label><b>Slug del rol: </b></label>
    <input type="text" class="form-control" value="{{ $rol[0]->slug }}" readonly="">
</div>

<div class="form-group">
    <label><b>Descripción del rol: </b></label>
    <textarea class="form-control" readonly="">{{ $rol[0]->descripcion }}</textarea>                                    
</div>

<div class="form-group">
    <label><b>Asociado a la Empresa:</b></label>
    <input type="text" class="form-control" value="{{ $rol[0]->nombre_empresa }}" readonly="">
</div>

<div class="form-group">
    <label><b>Estado de lal Rol: </b></label>
    @if($rol[0]->estado_rol == 1)
        <input class="form-control" type="text" value="Activo" readonly="">
    @else
        <input class="form-control" type="text" value="Inactivo" readonly="">
    @endif
</div>
