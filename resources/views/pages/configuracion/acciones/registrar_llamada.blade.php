<?php date_default_timezone_set('America/Bogota'); ?>


<form action="{{ url('/registro/acciones') }}" method="POST">
	@csrf
	@method('POST')
	<br>
	<input type="hidden" name="accion_realizada" value="{{ $valor }}">
	<input type="hidden" name="informacion_bitacora" value="{{ $informacion_bitacora[0]->codigo_bitacora }}">
	
	Realizar acción de: <b>Registrar llamada</b> al 
	<select class="form-control" name="celular">
		<option value="{{ $informacion_persona[0]->telefono_fijo }}">Telefono Fijo ({{ $informacion_persona[0]->telefono_fijo }})</option>
		<option value="{{ $informacion_persona[0]->celular_movil }}">Celular ({{ $informacion_persona[0]->celular_movil }})</option>
		<option value="{{ $informacion_persona[0]->celular_whatsapp }}">WhatsApp ({{ $informacion_persona[0]->celular_whatsapp }})</option>
	</select>
	<br>
	Hora inicio de llamada <input type="time" class="form-control" name="hora_inicio">
	<br>
	Hora final de llamada <input type="time" class="form-control" name="hora_fin">
	<br>
	Resultado de la llamada
	<textarea class="form-control" name="resultado_llamada"></textarea>
	<br>
	<div class="row">
		<div class="col">
			<b>Usuario que hará el registro</b>
			<input type="hidden" name="usuario_registro" value="{{ auth()->user()->id }}">
			<input type="text" class="form-control" value="{{ auth()->user()->name }}" readonly="">
		</div>
		<div class="col">
			<b>Fecha y hora del registro</b>
			<input type="text" class="form-control" name="fecha_registro" value="{{ date('Y-m-d H:i') }}" readonly="">
		</div>
	</div>
	<br><br>
	<center>
		<button type="submit" class="btn btn-success">Registrar acción realizada</button>
	</center>
</form>