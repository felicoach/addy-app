
<div class="modal fade text-left" id="informacion" tabindex="-1" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Informaci&oacute;n detallada</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal" >
                <p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <br><br>
                Cargando informaci&oacute;n...
                </p>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-danger waves-effect waves-float waves-light" data-dismiss="modal">Cerrar</button>
                </center>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="editar" tabindex="-1" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Edici&oacute;n Informaci&oacute;n.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal-edicion" >
                <p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <br /><br />
                Cargando informaci&oacute;n...
                </p>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-success waves-effect waves-float waves-light" id="creacion">Actualizar</button>
                    <button type="button" class="btn btn-danger waves-effect waves-float waves-light" data-dismiss="modal">Cerrar</button>
                </center>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="asignacion-permisos" tabindex="-1" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Asignaci&oacute;n de Permisos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal-asignacion" >
                <p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <br><br>
                Cargando informaci&oacute;n...
                </p>
            </div>
            <!--
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-danger waves-effect waves-float waves-light" data-dismiss="modal">Cerrar</button>
                </center>
            </div>-->
        </div>
    </div>
</div>

<div class="modal fade text-left" id="bitacoras" tabindex="-1" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Generar nueva Bitacora.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal-bitacora">
                <p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <br /><br />
                        Cargando informaci&oacute;n...
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="acciones" tabindex="-1" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Acción realizada.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal-acciones">
                <p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <br /><br />
                        Cargando informaci&oacute;n...
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="informacion_referido" tabindex="-1" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Información referido.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal-informacion_referido">
                <p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <br /><br />
                        Cargando informaci&oacute;n...
                </p>
            </div>
        </div>
    </div>
</div>
