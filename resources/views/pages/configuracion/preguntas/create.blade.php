
@extends('layouts.app')

@section('title', 'Registro Preguntas - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">

            @include('pages.configuracion.mensajes.modales')
            @include('pages.configuracion.mensajes.alertas')
            
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de preguntas</h3>
                            <p class="card-text font-small-3">
                                Bienvenido, desde esta sección puedes crear una nuevo Módulo en Addy</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>
                            
                            <form action="{{ route('preguntas.store') }}" method="POST">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                	<label><b>(*) Nombre usuario:</b></label>
                                	<input type="text" class="form-control" value="{{ auth()->user()->name }}" disabled="">
                                </div>

                                <div class="form-group">
                                	<label><b>(*) Descripción de la Pregunta:</b></label>
                                	<input type="text" class="form-control" name="pregunta" value="{{ old('pregunta') }}" >
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Asignar a la empresa:</b></label>
                                            <select class="form-control" name="empresas" id="selector_empresa">
                                                <option value="" selected=""></option>
                                                @foreach($empresas as $empresa)
                                                <option value="{{ $empresa->indentificacion_tributaria }}">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
                                                @endforeach
                                            </select>  
                                        </div>
                                        <div class="col">
                                            <label><b>(*) Asignar pregunta al Módulo: </b></label>
                                            <select class="form-control" name="modulo" id="selector">
                                            	<option value="" selected=""></option>
                                                @foreach($modulos as $modulo)
                                                	<option value="{{ $modulo->slug_modulo }}">{{ $modulo->nombre_modulo }}</option>
                                                @endforeach
                                            </select> 
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b></b>Código opcional: </label>
                                    		<input type="text" class="form-control" name="codigo_opcional">
                                        </div>
                                        <div class="col">
                                            <label><b>(*) Fecha: </b></label>
                                    		<input type="date" class="form-control" value="{{ date('Y-m-d') }}" disabled="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Estado de la Pregunta:</label>
                                    <select class="form-control" name="estado" value="{{ old('estado_empresa') }}">
                                        <option value="" selected="">Seleccione un estado</option>
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                                <br>
                                <center>
                                    <button type="submit" class="btn btn-primary">Registrar y volver</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection