<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AddyTipoCliente AS tipo_clientes;

class TipoClientesInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        
        tipo_clientes::create([
            'nombre_tipo'=>  'Cliente vendedor', 
            'slug_tipo' => 'cliente_vendedor', 
            'estado_tipo' => '1']);

        tipo_clientes::create([
            'nombre_tipo'=>  'Cliente Comprador', 
            'slug_tipo' => 'cliente_comprador', 
            'estado_tipo' => '1']);

    }
}
