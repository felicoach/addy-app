<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Empresa AS Empresas;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class EmpresaInfoSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Empresas::create([
            'logo_empresa' => 'https://lh3.google.com/u/0/d/1_iA-bao_0IWafupCHlVG_J8AP4N5sD54=w1920-h912-iv1',
            'favicon_empresa' => 'https://lh3.google.com/u/0/d/1_iA-bao_0IWafupCHlVG_J8AP4N5sD54=w1920-h912-iv1',
            'code_tipo_documento' => '2',
            'identificacion_tributaria' => '900534143',
            'nombre_empresa' => 'Addy',
            'email_empresa' => 'gerencia@addy.la',
            'descripcion_empresa' => 'Addy empresa',
            'telefono_principal' => '0324567889',
            'telefono_whatsapp' => '+573135523082',
            'palabras_claves' => 'Inmuebles',
            'acerca_de_negocio' => 'Addy empresas',
            'codigo_pais' => 'Co',
            'codigo_departamento' => '12',
            'codigo_ciudad' => '12',
            'codigo_postal' => '0',
            'direccion_empresa' => 'Calle 52 A # 4545',
            'estado_empresa' => '1',
        ]);
    }

}
