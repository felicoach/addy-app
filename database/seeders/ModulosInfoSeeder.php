<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AddyModulo           AS Modulos;
use App\Permisos\Models\Permission  AS Permisos;

class ModulosInfoSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        /* ================== Inicio Modulo de empresas ==================== */

            Modulos::create([
                'id_modulo' => '1',
                'icon_modulo' => '1',
                'nombre_modulo' => 'Empresas',
                'descripcion_modulo' => 'empresas',
                'slug_modulo' => 'empresas',
                'seccion_id' => '1',
                'estado_id' => '1',
            ]);
        
        /* ================= fin modulo empresas ========================== */

        /* ================== Inicio Modulo de Modulos ==================== */

            Modulos::create([
                'id_modulo' => '2',
                'icon_modulo' => '1',
                'nombre_modulo' => 'Modulos',
                'descripcion_modulo' => 'modulos',
                'slug_modulo' => 'modulos',
                'seccion_id' => '1',
                'estado_id' => '1',
            ]);
        
        /* ================== Inicio Modulo de Modulos ==================== */
                
        /* ================== Inicio Modulo de Roles ==================== */

            Modulos::create([
                'id_modulo' => '3',
                'icon_modulo' => '1',
                'nombre_modulo' => 'Roles',
                'descripcion_modulo' => 'roles',
                'slug_modulo' => 'roles',
                'seccion_id' => '1',
                'estado_id' => '1',
            ]);
        
        /* ================== fin Modulo de Roles ==================== */

        /* ================== Inicio Modulo de usuarios ==================== */

            Modulos::create([
                'id_modulo' => '4',
                'icon_modulo' => '1',
                'nombre_modulo' => 'Usuarios',
                'descripcion_modulo' => 'usuarios',
                'slug_modulo' => 'usuarios',
                'seccion_id' => '1',
                'estado_id' => '1',
            ]);

        /* ================== Fin Modulo de usuarios ==================== */

        /* ================== Inicio Modulo de personas ==================== */
         
            Modulos::create([
                'id_modulo' => '5',
                'icon_modulo' => '1',
                'nombre_modulo' => 'Personas',
                'descripcion_modulo' => 'personas',
                'slug_modulo' => 'personas',
                'seccion_id' => '1',
                'estado_id' => '1',
            ]);

        /* ================== Inicio Modulo de personas ==================== */

        /* ================== Inicio Modulo de referidos ==================== */

            Modulos::create([
                'id_modulo' => '6',
                'icon_modulo' => '1',
                'nombre_modulo' => 'Referidos',
                'descripcion_modulo' => 'referidos',
                'slug_modulo' => 'referidos',
                'seccion_id' => '1',
                'estado_id' => '1',
            ]);

        /* ================== Fin Modulo de referidos ==================== */

        /* ================== Inicio Modulo de CRM ==================== */

        Modulos::create([
            'id_modulo' => '7',
            'icon_modulo' => '1',
            'nombre_modulo' => 'CRM',
            'descripcion_modulo' => 'crm',
            'slug_modulo' => 'crm',
            'seccion_id' => '1',
            'estado_id' => '1',
        ]);

    /* ================== Fin Modulo de CRM ==================== */

    /* ================== Inicio Modulo de Inmuebles ==================== */

    Modulos::create([
        'id_modulo' => '8',
        'icon_modulo' => '1',
        'nombre_modulo' => 'Inmuebles',
        'descripcion_modulo' => 'inmuebles',
        'slug_modulo' => 'inmuebles',
        'seccion_id' => '1',
        'estado_id' => '1',
    ]);

/* ================== Fin Modulo de Inmuebles ==================== */

    }

}
