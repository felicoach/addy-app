<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\AddyEstado           AS Estados;
use App\Models\AddyAccione          AS Acciones;
use App\Permisos\Models\Permission  AS Permisos;
use App\Permisos\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PermisosInfoSeeder extends Seeder
{

    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate tables
        DB::statement("SET foreign_key_checks=0");
            DB::table('role_user')->truncate();
            DB::table('permission_role')->truncate();
            Permisos::truncate();
            Role::truncate();
        DB::statement("SET foreign_key_checks=1");

        //user admin
        $usuario_administrador= User::where('email','admin@admin.com')->first();
        if ($usuario_administrador) {
            $usuario_administrador->delete();
        }

        // ================== creacion de los usuarios ==================== //
        
            $usuario_administrador = User::create([
                'name'          => 'Administrador',
                'email'         => 'admin@admin.com',
                'password'      => Hash::make('admin'),
                'id_empresa'    => '1',
                'estado_user'   => '1'
            ]);


            // User::create([
            //     'name'          => 'Prueba',
            //     'email'         => 'admin@admin.com',
            //     'password'      => Hash::make('admin'),
            //     'id_empresa'    => '1',
            //     'estado_user'   => '1'
            // ]);

            $usuario_cliente = User::create([
                'name'          => 'cliente',
                'email'         => 'cliente@admin.com',
                'password'      => Hash::make('cliente'),
                'id_empresa'    => '1',
                'estado_user'   => '1'
            ]);

            

        // ================== fin creacion de los usuarios ==================== //
        
        // ================== Creacion de ROLES ==================== //

            $rol_administrador=Role::create([
                'id'    => '1',
                'nombre' => 'Administrador',
                'slug' => 'administrator',
                'descripcion' => 'Super usuario del sistema Addy',
                'full-access' => 'yes'
            ]);

            Role::create([
                'id'    => '2',
                'nombre' => 'Agentes',
                'slug' => 'agentes',
                'descripcion' => 'Agentes, tendrás mucho más acceso en Addy. Se dará permisos para crear, comprar referidos en nuestra plaforma.',
                'full-access' => 'no'
            ]);
            
            $rol_cliente = Role::create([
                'id'    => '3',
                'nombre' => 'Socio referidor',
                'slug' => 'socio_referidor',
                'descripcion' => 'Socio referidor, solo tendrá permisos para referir nuevas personas.',
                'full-access' => 'no'
            ]);

            Role::create([
                'id'    => '4',
                'nombre' => 'Visitantes',
                'slug' => 'visitantes',
                'descripcion' => 'Visitante, solo podrás entrar y actualizar el perfil.',
                'full-access' => 'no'
            ]);

        // ================== fin de los roles ==================== //


        // ================== CREACION DE PERMISOS ================== //
        
            // ==================  Modulo de Permisos ==================== //


                $permisos = Permisos::create([
                    'id'    =>  '1',
                    'name'        => 'Crear Permiso',
                    'slug'          => 'permisos.crear',
                    'description'   => 'El usuario podrá crear un Permisos en el Sistema.',
                    'slug_vue'    => 'create',
                    'name_module'         => 'permisos',
                ]);
                $permisos_all[] = $permisos->id;


                $permisos = Permisos::create([
                    'id'    =>  '2',
                    'name' => 'ver información de Permiso',
                    'slug' => 'permisos.ver',
                    'description' => 'El usuario podrá ver modal información del permiso seleccionado.',
                    'slug_vue'    => 'read',
                    'name_module' => 'permisos',
                ]);
                $permisos_all[] = $permisos->id;
                

                $permisos = Permisos::create([
                    'id'    =>  '3',
                    'name' => 'Edición de Permisos',
                    'slug' => 'permisos.edicion',
                    'description' => 'El usuario podrá editar en una modal información del permiso seleccionado.',
                    'slug_vue'    => 'update',
                    'name_module' => 'permisos',
                ]);
                $permisos_all[] = $permisos->id;


                $permisos = Permisos::create([
                    'id'    =>  '4',
                    'name' => 'Eliminar un Permiso',
                    'slug' => 'permisos.eliminar',
                    'description' => 'El usuario podrá eliminar un permiso seleccionado.',
                    'slug_vue'    => 'delete',
                    'name_module' => 'permisos',
                ]);
                $permisos_all[] = $permisos->id;
            // ==================  Modulo de Permisos * ------ //

            // ================== Inicio Modulo de referidos ==================== //
                $permisos = Permisos::create([
                    'id'            => '5',
                    'name'        => 'Crear referidos',
                    'slug'          => 'referidos.crear',
                    'description'   => 'El usuario podrá crear un Permreferidosireferidossos en el Sistema.',
                    'slug_vue'    => 'create',
                    'name_module'         => 'referidos',
                ]);
                $permisos_all[] = $permisos->id;


                $permisos = Permisos::create([
                    'id'            => '6',
                    'name' => 'ver información de referidos',
                    'slug' => 'referidos.ver',
                    'description' => 'El usuario podrá ver modal información del referidos seleccionado.',
                    'slug_vue'    => 'read',
                    'name_module' => 'referidos',
                ]);
                $permisos_all[] = $permisos->id;
                

                $permisos = Permisos::create([
                    'id'            => '7',
                    'name' => 'Edición de referidos',
                    'slug' => 'referidos.edicion',
                    'description' => 'El usuario podrá editar en una modal información del referidos seleccionado.',
                    'slug_vue'    => 'update',
                    'name_module' => 'referidos',
                ]);
                $permisos_all[] = $permisos->id;


                $permisos = Permisos::create([
                    'id'            => '8',
                    'name' => 'Eliminar un referidos',
                    'slug' => 'referidos.eliminar',
                    'description' => 'El usuario podrá eliminar un referidos seleccionado.',
                    'slug_vue'    => 'delete',
                    'name_module' => 'referidos',
                ]);
                $permisos_all[] = $permisos->id;


            // ================== Fin Modulo de referidos ==================== //
       
            // ====================  Modulo de Perfil ==================== //
            
                $permisos = Permisos::create([
                    'id'            => '9',
                    'name'        => 'Crear perfil',
                    'slug'          => 'perfil.crear',
                    'description'   => 'El usuario podrá crear un perfil en el Sistema.',
                    'slug_vue'    => 'create',
                    'name_module'         => 'perfil',
                ]);
                $permisos_all[] = $permisos->id;


                $permisos = Permisos::create([
                    'id'            => '10',
                    'name'          => 'ver información de perfil ',
                    'slug'          => 'perfil.ver',
                    'description'   => 'El usuario podrá ver modal información del perfil  seleccionado.',
                    'slug_vue'      => 'read',
                    'name_module'   => 'perfil',
                ]);
                $permisos_all[] = $permisos->id;
                

                $permisos = Permisos::create([
                    'id'            => '11',
                    'name' => 'Edición de perfil',
                    'slug' => 'perfil.edicion',
                    'description' => 'El usuario podrá editar en una modal información del perfil  seleccionado.',
                    'slug_vue'    => 'update',
                    'name_module' => 'perfil',
                ]);
                $permisos_all[] = $permisos->id;


                $permisos = Permisos::create([
                    'id'            => '12',
                    'name' => 'Eliminar un perfil ',
                    'slug' => 'perfil.eliminar ',
                    'description' => 'El usuario podrá eliminar un perfil  seleccionado.',
                    'slug_vue'    => 'delete',
                    'name_module' => 'perfil',
                ]);
                $permisos_all[] = $permisos->id;
            // ================== Fin Modulo de PERFIL ==================== //
                
        
        // ================== FIN CREACION DE PERMISOS ================== //



        // ================== asignacion de permisos ==================== //
        // ================== fin asignacion de permisos ==================== //

        // ================== Lista de datos  ==================== //

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Cédula ciudadania',
            'abreviado_tipo'   => 'CC',
            'estado_tipo'   =>  '1',
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Número de identificación tributaria',
            'abreviado_tipo'   => 'NIT',
            'estado_tipo'   =>  '1',
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Id',
            'abreviado_tipo'   => 'ID',
            'estado_tipo'   =>  '1',
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Cedula de extrangeria',
            'abreviado_tipo'   => 'CE',
            'estado_tipo'   =>  '1',
        ]);

        DB::table('tipo_identificions')->insert([
            'nombre_tipo'   => 'Pasaporte',
            'abreviado_tipo'   => 'PASS',
            'estado_tipo'   =>  '1',
        ]);
        // ================== Fin Lista de datos  ==================== //

        Estados::create([
            'nombre_estado'         => 'Activo',
            'descripcion_estado'    => 'Activo en el sistema',
            'modulo_estado'         => '0',
        ]);

        Estados::create([
            'nombre_estado'         => 'Inactivo',
            'descripcion_estado'    => 'Inactivo/Eliminado en el sistema',
            'modulo_estado'         => '0',
        ]);

        Estados::create([
            'nombre_estado'         => 'Perfil Registrado - Incompleto',
            'descripcion_estado'    => 'Perfil registrado pero aun no llena el formulario',
            'modulo_estado'         => 'usuarios',
        ]);

        Estados::create([
            'nombre_estado'         => 'Bitacora Iniciada',
            'descripcion_estado'    => 'Bitacora iniciada',
            'modulo_estado'         => 'bitacoras',
        ]);

        Estados::create([
            'nombre_estado'         => 'Bitacora en Proceso',
            'descripcion_estado'    => 'Bitacora iniciada',
            'modulo_estado'         => 'bitacoras',
        ]);

        Estados::create([
            'nombre_estado'         => 'Bitacora terminada',
            'descripcion_estado'    => 'Bitacora se ha terminado',
            'modulo_estado'         => 'bitacoras',
        ]);

        Acciones::create([
            'codigo_accion'         => '0',
            'descripcion_accion'    => 'Sin acción previa',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '1',
            'descripcion_accion'    => 'Registrar llamada',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '2',
            'descripcion_accion'    => 'Registrar cita programada',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '3',
            'descripcion_accion'    => 'Registrar presentación de compradores',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        Acciones::create([
            'codigo_accion'         => '4',
            'descripcion_accion'    => 'Registrar un toque',
            'modulo_accion'         => 'bitacoras',
            'id_estado_accion'      => '1',
        ]);

        DB::table('addy_detalle_roles_empresas')->insert([
            'empresa_id'    => '1',
            'role_id'       =>  '2',
        ]);

        //table role_user
        $usuario_administrador->roles()->sync([ $rol_administrador->id ]);
        $usuario_cliente->roles()->sync([ $rol_cliente->id ]);


        //Permisos
        $permisos_all = [];

    


        // ------ * Modulo de perguntas * ------ //


        $permisos = Permisos::create([
            'id'            => '13',
            'name'          => 'Crear preguntas',
            'slug'          => 'preguntas.crear',
            'description'    => 'El usuario podrá crear un Permreferidosireferidossos en el Sistema.',
            'slug_vue'       => 'create',
            'name_module'    => 'preguntas',
        ]);
        $permisos_all[] = $permisos->id;


        $permisos = Permisos::create([
            'id'            => '14',
            'name' => 'ver información de preguntas',
            'slug' => 'preguntas.ver',
            'description' => 'El usuario podrá ver modal información del preguntas seleccionado.',
            'slug_vue'    => 'read',
            'name_module' => 'preguntas',
        ]);
        $permisos_all[] = $permisos->id;
        

        $permisos = Permisos::create([
            'id'            => '15',
            'name' => 'Edición de preguntas',
            'slug' => 'preguntas.edicion',
            'description' => 'El usuario podrá editar en una modal información del preguntas seleccionado.',
            'slug_vue'    => 'update',
            'name_module' => 'preguntas',
        ]);
        $permisos_all[] = $permisos->id;


        $permisos = Permisos::create([
            'id'            => '16',
            'name' => 'Eliminar un preguntas',
            'slug' => 'preguntas.eliminar',
            'description' => 'El usuario podrá eliminar un preguntas seleccionado.',
            'slug_vue'    => 'delete',
            'name_module' => 'preguntas',
        ]);
        $permisos_all[] = $permisos->id;

        
            // ================== Ini CRM ==================== //
            $permisos = Permisos::create([
                'id'            => '17',
                'name'        => 'Crear crm',
                'slug'          => 'crm.crear',
                'description'   => 'El usuario podrá crear un registro en crm.',
                'slug_vue'    => 'create',
                'name_module'         => 'crm',
            ]);
            $permisos_all[] = $permisos->id;


            $permisos = Permisos::create([
                'id'            => '18',
                'name' => 'ver crm',
                'slug' => 'crm.ver',
                'description' => 'El usuario podrá ver los datos del crm.',
                'slug_vue'    => 'read',
                'name_module' => 'crm',
            ]);
            $permisos_all[] = $permisos->id;
            

            $permisos = Permisos::create([
                'id'            => '19',
                'name' => 'Edición de crm',
                'slug' => 'crm.edicion',
                'description' => 'El usuario podrá editar info del crm.',
                'slug_vue'    => 'update',
                'name_module' => 'crm',
            ]);
            $permisos_all[] = $permisos->id;


            $permisos = Permisos::create([
                'id'            => '20',
                'name' => 'Eliminar un crm',
                'slug' => 'crm.eliminar',
                'description' => 'El usuario podrá eliminar regoistro del crm.',
                'slug_vue'    => 'delete',
                'name_module' => 'crm',
            ]);
            $permisos_all[] = $permisos->id;
            // ================== Fin CRM ==================== //

            // ================== Ini INMUEBLES ==================== //
            $permisos = Permisos::create([
                'id'            => '21',
                'name'        => 'Crear inmuebles',
                'slug'          => 'inmuebles.crear',
                'description'   => 'El usuario podrá crear un registro en inmuebless.',
                'slug_vue'    => 'create',
                'name_module'         => 'inmuebles',
            ]);
            $permisos_all[] = $permisos->id;


            $permisos = Permisos::create([
                'id'            => '22',
                'name' => 'ver inmuebles',
                'slug' => 'inmuebles.ver',
                'description' => 'El usuario podrá ver los datos del inmuebles.',
                'slug_vue'    => 'read',
                'name_module' => 'inmuebles',
            ]);
            $permisos_all[] = $permisos->id;
            

            $permisos = Permisos::create([
                'id'            => '23',
                'name' => 'Edición de inmuebles',
                'slug' => 'inmuebles.edicion',
                'description' => 'El usuario podrá editar info del inmuebles.',
                'slug_vue'    => 'update',
                'name_module' => 'inmuebles',
            ]);
            $permisos_all[] = $permisos->id;


            $permisos = Permisos::create([
                'id'            => '24',
                'name' => 'Eliminar un inmuebles',
                'slug' => 'inmuebles.eliminar',
                'description' => 'El usuario podrá eliminar regoistro del inmuebles.',
                'slug_vue'    => 'delete',
                'name_module' => 'inmuebles',
            ]);
            $permisos_all[] = $permisos->id;

            // ================== Fin INMUEBLES ==================== //

    // ------ * Modulo de perguntas * ------ //



        
        

        /* Seeder configuracion de Roles */
        
        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '9',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '9',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '10',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '11',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '5',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '6',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '7',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '13',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '2',
            'permission_id' =>  '14',
        ]);

        /* ASIGNACION DE PERMISOS PERFIL AL ROL SOCIO REFERIDOR */
        
        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '5',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '6',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '7',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '8',
        ]);
        
        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '9',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '10',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '11',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '12',
        ]);

        // INI ROLES PARA CRM
        
        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '17',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '18',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '19',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '20',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '21',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '22',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '23',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '3',
            'permission_id' =>  '24',
        ]);
            
        // Fin Roles en CRM


        /* FIN ASIGNACION DE PERMISOS PERFIL AL ROL REFERIDOR */



        /* ASIGNACION DE PERMISOS PERFIL AL ROL VISITANTE */

        DB::table('permission_role')->insert([
            'role_id'       =>  '4',
            'permission_id' =>  '9',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '4',
            'permission_id' =>  '10',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '4',
            'permission_id' =>  '11',
        ]);

        DB::table('permission_role')->insert([
            'role_id'       =>  '4',
            'permission_id' =>  '12',
        ]);

    }   
}
