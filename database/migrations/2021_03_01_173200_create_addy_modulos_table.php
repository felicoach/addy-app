<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_modulos', function (Blueprint $table) {
            $table->increments('id_modulo');
            $table->char('icon_modulo', 150);
            $table->char('nombre_modulo', 150);
            $table->longText('descripcion_modulo')->default('Descripcion Modulo Addy')->nullable();
            $table->string('slug_modulo', 250)->unique();
            $table->integer('seccion_id');
            $table->integer('estado_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_modulos');
    }
}
