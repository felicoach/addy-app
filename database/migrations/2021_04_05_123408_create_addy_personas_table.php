<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('addy_personas', function (Blueprint $table) {
            $table->id();
            $table->integer('tipo_identificacion')->length(12)->unsigned()->nullable(false);
            $table->longText('foto_persona')->nullable();
            $table->bigInteger('cedula_persona')->unique();
            $table->char('primer_nombre', 150);
            $table->char('segundo_nombre', 150)->nullable();
            $table->char('primer_apellido', 150);
            $table->char('segundo_apellido', 150)->nullable();
            $table->char('fecha_nacimiento', 50)->nullable();
            $table->char('correo_persona', 100)->unique();
            $table->char('telefono_fijo', 20)->nullable();
            $table->text('celular_movil', 20)->nullable();
            $table->char('celular_whatsapp', 20);
            $table->longText('direccion_persona')->nullable();
            $table->integer('codigo_ciudad')->length(12)->unsigned()->nullable();
            $table->char('codigo_pais', 10)->nullable();
            $table->integer('codigo_postal')->length(12)->unsigned()->nullable();
            $table->integer('id_perfil')->length(12)->unsigned()->nullable(false);
            $table->integer('id_usuario')->length(12)->unsigned()->nullable(false);
            $table->integer('estado_persona')->length(12)->unsigned()->nullable(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_personas');
    }
}
