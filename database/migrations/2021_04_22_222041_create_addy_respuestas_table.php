<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('addy_respuestas', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('id_pregunta')->references('id')->on('addy_preguntas')->onDelete('cascade'); //Foraneas
            $table->longText('descripcion_respuesta')->nullable();
            $table->integer('valor_respuesta')->length(12)->unsigned()->nullable(false);
            $table->integer('estado_respuesta')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_respuestas');
    }
}
