<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyDetalleDocumentoReferidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_detalle_documento_referidos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cedula_referidos_documento')->length(12)->unsigned()->nullable(false);
            $table->integer('id_documento')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_detalle_documento_referidos');
    }
}
