<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyReferidosPublicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_referidos_publicos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_referido')->length(12)->unsigned()->nullable(false);
            $table->char('nit_empresa', 25)->nullable(false);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_referidos_publicos');
    }
}
