<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\PermisosController 	AS Permisos;
use App\Http\Controllers\ModulosController 		AS Modulos;
use App\Http\Controllers\RolesController 		AS Roles;
use App\Http\Controllers\Auth\LoginController	AS Login;
use App\Http\Controllers\UserController 		AS Usuarios;
use App\Http\Controllers\PersonasController 	AS Personas;
use App\Http\Controllers\ReferidoController 	AS Referidos;
use App\Http\Controllers\PreguntasController 	AS Preguntas;
use App\Http\Controllers\BitacorasController 	AS Bitacoras;
use App\Http\Controllers\HomeController			AS Home;

use App\Http\Controllers\ApplicationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('login/{provider}', [Login::class, 'redirectToProvider'])->name('social.auth');
Route::get('login/{provider}/callback', [Login::class, 'handleProviderCallback'])->name('social.callback');

Auth::routes();

Route::get('/', function () {
	if(Auth::guest()){
    	return view('auth.login');
	}else{
        return redirect()->route('home')->with('status_success', 'Se ha iniciado sesion recientemente');
	}
});

//Route::get('/home', [Home::class, 'index'])->name('home');



Route::get('/perfil', [Personas::class, 'perfil'])->name('perfil');
Route::get('/perfil/usuario', [Personas::class, 'perfilUnico'])->name('perfin.usuario');
Route::post('/perfil/store', [Personas::class, 'perfilStore'])->name('perfil.store');


Route::resource('/permisos', Permisos::class)->names('permisos');


Route::resource('/modulos', Modulos::class)->names('modulos');


Route::resource('/usuarios', Usuarios::class)->names('users');

Route::get('/asignacion_permisos/update/{id?}', [Permisos::class, 'edit'])->name('asignacion.edit');
Route::post('/asignacion_permisos/update/permiso/{id?}', [Permisos::class, 'update'])->name('asignacion.update');


Route::resource('/personas', Personas::class)->names('personas');

Route::resource('/referidos', Referidos::class)->names('referidos');


Route::get('/tipo_clientes/{id}', [Referidos::class, 'tipo_clientes']);

Route::resource('/preguntas', Preguntas::class)->names('preguntas');

Route::resource('/bitacoras', Bitacoras::class)->names('bitacoras');
Route::get('/generar/{cedula}', [Bitacoras::class, 'generar'])->name('generar');
Route::get('/generar/acciones/{valor}/{bitacora}', [Bitacoras::class, 'acciones'])->name('acciones');
Route::post('/registro/acciones', [Bitacoras::class, 'registro']);
Route::get('/informacion/referido/{cedula}', [Bitacoras::class, 'informacion_referido']);


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::post('/login', [Login::class, 'login'])->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout')->middleware('auth:api');
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');*/



/*
|--------------------------------------------------------------------------
| Laravel Passport Routes
|--------------------------------------------------------------------------
|
| If you'd like to use full functionality of laravel passport
| use the routes below instead of registering your routes in
| AuthServiceProvider as instructed in passport documentation.
| This will use "auth:api" guard instead of "auth"
|

Route::get('oauth/authorize', '\Laravel\Passport\Http\Controllers\AuthorizationController@authorize')->middleware('auth:api');
Route::delete('oauth/authorize', '\Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny')->middleware('auth:api');
Route::post('oauth/authorize', '\Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve')->middleware('auth:api');

Route::get('oauth/clients', '\Laravel\Passport\Http\Controllers\ClientController@forUser')->middleware('auth:api');
Route::post('oauth/clients', '\Laravel\Passport\Http\Controllers\ClientController@store')->middleware('auth:api');
Route::put('oauth/clients/{client_id}', '\Laravel\Passport\Http\Controllers\ClientController@update')->middleware('auth:api');
Route::delete('oauth/clients/{client_id}', '\Laravel\Passport\Http\Controllers\ClientController@destroy')->middleware('auth:api');

Route::post('oauth/personal-access-tokens', '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store')->middleware('auth:api');
Route::get('oauth/personal-access-tokens', '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser')->middleware('auth:api');
Route::delete('oauth/personal-access-tokens/{token_id}', '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy')->middleware('auth:api');

Route::get('oauth/scopes', '\Laravel\Passport\Http\Controllers\ScopeController@all')->middleware('auth:api');
Route::post('oauth/token/refresh', '\Laravel\Passport\Http\Controllers\TransientTokenController@refresh')->middleware('auth:api');
Route::get('oauth/tokens', '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser')->middleware('auth:api');
Route::delete('oauth/tokens/{token_id}', '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy')->middleware('auth:api');

*/

//Auth::routes();
//Route::get('oauth/scopes', '\Laravel\Passport\Http\Controllers\ScopeController@all')->middleware('auth:api');

Route::get('/{any}', [ApplicationController::class, 'index'])->where('any', '.*');
