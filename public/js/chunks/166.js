(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[166],{

/***/ "./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue":
/*!*********************************************************************!*\
  !*** ./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmpresaAdd_vue_vue_type_template_id_2f5f4b7d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmpresaAdd.vue?vue&type=template&id=2f5f4b7d& */ "./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=template&id=2f5f4b7d&");
/* harmony import */ var _EmpresaAdd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmpresaAdd.vue?vue&type=script&lang=js& */ "./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EmpresaAdd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmpresaAdd_vue_vue_type_template_id_2f5f4b7d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmpresaAdd_vue_vue_type_template_id_2f5f4b7d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmpresaAdd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmpresaAdd.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmpresaAdd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=template&id=2f5f4b7d&":
/*!****************************************************************************************************!*\
  !*** ./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=template&id=2f5f4b7d& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmpresaAdd_vue_vue_type_template_id_2f5f4b7d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmpresaAdd.vue?vue&type=template&id=2f5f4b7d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=template&id=2f5f4b7d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmpresaAdd_vue_vue_type_template_id_2f5f4b7d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmpresaAdd_vue_vue_type_template_id_2f5f4b7d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vee-validate */ "./frontend/node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var _store_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/index */ "./frontend/src/store/index.js");
/* harmony import */ var _vue_composition_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vue/composition-api */ "./frontend/node_modules/@vue/composition-api/dist/vue-composition-api.esm.js");
/* harmony import */ var _empresaStoreModule__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../empresaStoreModule */ "./frontend/src/views/apps/empresas/empresaStoreModule.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      empresa: {
        acerca_de_negocio: "addfghd",
        code_tipo_documento: 2,
        codigo_ciudad: 1,
        codigo_departamento: 2,
        codigo_pais: 1,
        codigo_postal: 1234,
        descripcion_empresa: "especialidad",
        direccion_empresa: "calle2",
        email_empresa: "empresa@empresa.com",
        estado_empresa: 2,
        favicon_empresa: null,
        identificacion_tributaria: 900594143,
        logo_empresa: null,
        nombre_empresa: "andres",
        palabras_claves: ["prueba"],
        telefono_principal: 12345645,
        telefono_whatsapp: 4323435
      }
    };
  },
  methods: {
    logoImage: function logoImage(event) {
      this.empresa.logo_empresa = event.target.file[0];
    },
    faviconImage: function faviconImage(event) {
      var input = event.target;
      var formData = new FormData();
      formData.append("file", input);
      this.empresa.favicon_empresa = formData;
    },
    validationForm: function validationForm() {
      var EMPRESA_APP_STORE_MODULE_NAME = "app-empresa"; // Register module

      if (!_store_index__WEBPACK_IMPORTED_MODULE_1__["default"].hasModule(EMPRESA_APP_STORE_MODULE_NAME)) _store_index__WEBPACK_IMPORTED_MODULE_1__["default"].registerModule(EMPRESA_APP_STORE_MODULE_NAME, _empresaStoreModule__WEBPACK_IMPORTED_MODULE_3__["default"]); // UnRegister on leave

      Object(_vue_composition_api__WEBPACK_IMPORTED_MODULE_2__["onUnmounted"])(function () {
        if (_store_index__WEBPACK_IMPORTED_MODULE_1__["default"].hasModule(EMPRESA_APP_STORE_MODULE_NAME)) _store_index__WEBPACK_IMPORTED_MODULE_1__["default"].unregisterModule(EMPRESA_APP_STORE_MODULE_NAME);
      });
      _store_index__WEBPACK_IMPORTED_MODULE_1__["default"].dispatch("app-empresa/addEmpresa", this.empresa).then(function (response) {
        console.log(response);
      })["catch"](function (error) {
        if (error.response.status === 404) {//userData.value = undefined;
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=template&id=2f5f4b7d&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/src/views/apps/empresas/empresa-add/EmpresaAdd.vue?vue&type=template&id=2f5f4b7d& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-body" }, [
    _c("section", { attrs: { id: "dashboard-ecommerce" } }, [
      _c("div", { staticClass: "row match-height" }, [
        _c("div", { staticClass: "col-12 col-sm-12 col-xl-12 col-lg-12" }, [
          _c("div", { staticClass: "card card-congratulation-medal" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("h3", [_vm._v("Registro de una empresa")]),
              _vm._v(" "),
              _c("p", { staticClass: "card-text font-small-3" }, [
                _vm._v(
                  "\n              Bienvenido, desde esta sección puedes crear una nueva Empresa en\n              Addy\n            "
                )
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c(
                "form",
                {
                  attrs: { enctype: "multipart/form-data" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                    }
                  }
                },
                [
                  _c("div", { staticClass: "form-group" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("input", {
                      ref: "file",
                      staticClass: "form-control",
                      attrs: {
                        type: "file",
                        id: "file",
                        name: "file",
                        accept: "image/*"
                      },
                      on: { change: _vm.logoImage }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _vm._m(1),
                    _vm._v(" "),
                    _c("input", {
                      ref: "file",
                      staticClass: "form-control",
                      attrs: {
                        type: "file",
                        id: "file",
                        name: "file",
                        accept: "image/*"
                      },
                      on: { change: _vm.faviconImage }
                    })
                  ]),
                  _vm._v(" "),
                  _vm._m(2),
                  _vm._v(" "),
                  _vm._m(3),
                  _vm._v(" "),
                  _vm._m(4),
                  _vm._v(" "),
                  _vm._m(5),
                  _vm._v(" "),
                  _vm._m(6),
                  _vm._v(" "),
                  _vm._m(7),
                  _vm._v(" "),
                  _vm._m(8),
                  _vm._v(" "),
                  _vm._m(9),
                  _vm._v(" "),
                  _vm._m(10),
                  _vm._v(" "),
                  _c("center", [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary",
                        attrs: { type: "submit" },
                        on: { click: _vm.validationForm }
                      },
                      [
                        _vm._v(
                          "\n                  Registrar Empresa\n                "
                        )
                      ]
                    )
                  ])
                ],
                1
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("* ")]), _vm._v("Logo de la empresa")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("* ")]), _vm._v("Favicon Empresa")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-3" }, [
          _c("label", [
            _c("b", [_vm._v("* ")]),
            _vm._v("Tipo de Identificación:")
          ]),
          _vm._v(" "),
          _c(
            "select",
            {
              staticClass: "form-control",
              attrs: { name: "code_tipo_documento" }
            },
            [
              _c("option", { attrs: { value: "" } }),
              _vm._v(" "),
              _c("option", { attrs: { value: "1" } }, [_vm._v("CC")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "2" } }, [_vm._v("NIT")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "3" } }, [_vm._v("RUT")])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-7" }, [
          _c("label", [
            _c("b", [_vm._v("* ")]),
            _vm._v("Número de Identificación:")
          ]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: {
              type: "text",
              name: "indentificacion_tributaria",
              value: "",
              id: "nro_identificacion"
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-2" }, [
          _c("label", [
            _c("b", [_vm._v("* ")]),
            _vm._v("No. de verificación:")
          ]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: {
              type: "text",
              name: "digito_verificacion",
              readonly: "",
              value: "",
              id: "digito_verificacion"
            }
          })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", [_c("b", [_vm._v("* ")]), _vm._v("Razón social:")]),
      _vm._v(" "),
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "text", name: "nombre_empresa", value: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", [_c("b", [_vm._v("* ")]), _vm._v("Email empresa:")]),
      _vm._v(" "),
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "email", name: "email_empresa", id: "email", value: "" }
      }),
      _vm._v(" "),
      _c(
        "code",
        { staticStyle: { display: "none" }, attrs: { id: "error-email" } },
        [_vm._v("Debes de digitar un correo válido.")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("Descripción de la empresa:")]),
          _vm._v(" "),
          _c("textarea", {
            staticClass: "form-control",
            attrs: { name: "descripcion_empresa" }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("Acerca de su negocio:")]),
          _vm._v(" "),
          _c("textarea", {
            staticClass: "form-control",
            attrs: { name: "acerca_negocio" }
          })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("Teléfono fijo:")]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: { type: "number", name: "telefono_fijo", value: "" }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("WhatsApp:")]),
          _c("br"),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: {
              type: "tel",
              name: "telefono_whatsapp",
              id: "celular_movil",
              value: ""
            }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "hide", attrs: { id: "valid-msg" } }, [
            _vm._v("✓ Válido")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "hide", attrs: { id: "error-msg" } })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("Dirección de la empresa:")]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: { type: "text", name: "direccion_empresa", value: "" }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("Código Postal:")]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: { type: "number", name: "codigo_postal", value: "" }
          })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", [_vm._v("Palabras claves:")]),
      _vm._v(" "),
      _c("select", {
        staticClass: "form-control palabras_claves",
        attrs: { multiple: "multiple", name: "palabras_claves[]" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("País de la empresa:")]),
          _vm._v(" "),
          _c(
            "select",
            {
              staticClass: "form-control",
              attrs: { id: "selector", name: "codigo_pais", value: "" }
            },
            [
              _c("option", { attrs: { value: "", selected: "" } }),
              _vm._v(" "),
              _c("option", { attrs: { value: "" } })
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col" }, [
          _c("label", [_vm._v("Ciudad de la empresa:")]),
          _vm._v(" "),
          _c("select", {
            staticClass: "form-control",
            attrs: { id: "selector_ciudad", name: "codigo_ciudad", value: "" }
          })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", [_vm._v("Estado de la Empresa:")]),
      _vm._v(" "),
      _c(
        "select",
        {
          staticClass: "form-control",
          attrs: { name: "estado_empresa", value: "" }
        },
        [
          _c("option", { attrs: { value: "" } }, [
            _vm._v("Seleccione un estado")
          ]),
          _vm._v(" "),
          _c("option", { attrs: { value: "1" } }, [_vm._v("Activo")]),
          _vm._v(" "),
          _c("option", { attrs: { value: "2" } }, [_vm._v("Inactivo")])
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ })

}]);